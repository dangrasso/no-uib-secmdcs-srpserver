# SrpServer #

Secure Remote Password server-side module for Google AppEngine.

-Based on BouncyCastle for the crypto primitives.

### Example ###
you can *hopefully* find a running instance @ [http://no-uib-secmdcs-srpserver.appspot.com/](http://no-uib-secmdcs-srpserver.appspot.com/)