package no.uib.secmdcs.srpserver.persistence.dao;

import java.math.BigInteger;
import java.util.List;

import no.uib.secmdcs.srpserver.model.SrpUserAccount;
import no.uib.secmdcs.srpserver.persistence.EMFService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;


public enum SrpUserAccountDao {
	INSTANCE;

	public List<SrpUserAccount> listUserAccounts() {
		EntityManager em = EMFService.get().createEntityManager();
		// Read the existing entries
		Query q = em.createQuery("select u from SrpUserAccount u");
		@SuppressWarnings("unchecked")
		List<SrpUserAccount> users = q.getResultList();
		return users;
	}

	/**
	 * Creates a new user and persists its data. If the user is already existing, throws a PersistenceException
	 * @param userId
	 * @param verifier
	 * @param salt
	 */
	public void add(String userId, BigInteger verifier, byte[] salt) {
		EntityManager em = EMFService.get().createEntityManager();
		synchronized (this) {
			SrpUserAccount user = em.find(SrpUserAccount.class, userId);
			if (user==null) {
				user = new SrpUserAccount(userId, verifier, salt);
				em.persist(user);
				em.close();
			} else {
				em.close();
				throw new PersistenceException("The user '" + userId + "' already exists!");
			}
		}
	}

	/**
	 * Creates or updates the specified user data in the persistence
	 * @param userId
	 * @param verifier
	 * @param salt
	 */
	public void save(String userId, BigInteger verifier, byte[] salt) {
		EntityManager em = EMFService.get().createEntityManager();
		synchronized (this) {
			SrpUserAccount user = new SrpUserAccount(userId, verifier, salt);
			em.persist(user);
			em.close();
		}
	}

	public SrpUserAccount get(String userId) {
		EntityManager em = EMFService.get().createEntityManager();
		return em.find(SrpUserAccount.class, userId);
		//		Query q = em
		//				.createQuery("select u from SrpUser u where u.userId = :userId");
		//		q.setParameter("userId", userId);
		//		List<SrpUser> results = q.getResultList();
		//		if (!results.isEmpty())
		//			return (SrpUser) results.get(0);
		//		return null;
	}

	public void remove(String userId) {
		EntityManager em = EMFService.get().createEntityManager();
		try {
			Query q = em
					.createQuery("delete from SrpUserAccount u where u.userId = :userId");
			q.setParameter("userId", userId);
			q.executeUpdate();
		} finally {
			em.close();
		}
	}
} 