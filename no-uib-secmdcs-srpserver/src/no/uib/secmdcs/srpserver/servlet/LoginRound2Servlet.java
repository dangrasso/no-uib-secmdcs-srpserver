package no.uib.secmdcs.srpserver.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import no.uib.secmdcs.srpserver.crypto.SrpException;
import no.uib.secmdcs.srpserver.crypto.SrpServerContext;
import no.uib.secmdcs.srpserver.crypto.SrpServerProviderFactory;
import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class LoginRound2Servlet extends HttpServlet {

	private static final long serialVersionUID = -2296739364573682508L;

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		HttpSession s = req.getSession();

		if (s.getAttribute("SRP_SERVER_CONTEXT")==null) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
					"Srp server context not found in the session at round 2"); 

			s.invalidate();
			return;
		}
		SrpServerContext srpCtx = (SrpServerContext) s.getAttribute("SRP_SERVER_CONTEXT");

		//receive parameters from request JSON body
		StringBuilder sb = new StringBuilder();
		BufferedReader reader = req.getReader();
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				sb.append(line).append('\n');
			}
		} finally {
			reader.close();
		}
		JSONObject jsonReq;
		BigInteger A = null;
		BigInteger M1 = null;
		try {
			jsonReq = new JSONObject(sb.toString());
			//check request parameters
			if (!jsonReq.has("SRP_A")){
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
						"Server: missing 'SRP_A' request parameter for round 2" ); 
				s.invalidate();
				return;
			}
			if (!jsonReq.has("SRP_M1")){
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
						"Server: missing 'SRP_M1' request parameter for round 2" ); 
				s.invalidate();
				return;
			}
			A = new BigInteger(jsonReq.getString("SRP_A"));
			M1 = new BigInteger(jsonReq.getString("SRP_M1"));
		} catch (JSONException e) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Server: error while parsing JSON request parameters for round 2" ); 
			s.invalidate();
			return;
		} catch (NumberFormatException e) {
			resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
					"Server: invalid number format in JSON request parameters for round 2"); 
			s.invalidate();
			return;
		}

		//create the SRP provider
		ISrpServerProvider srp = null;
		srp = SrpServerProviderFactory.getSrpServerProvider();

		//Round2 crypto...
		try {
			srpCtx = srp.serverRound2(A, M1, srpCtx);
		} catch (SrpException e) {
			if (e.getType().equals(SrpException.Type.INVALID_CREDENTIALS)) {
				resp.sendError(HttpServletResponse.SC_UNAUTHORIZED, 
						e.getType().toString() + ": " + e.getMessage()); 
				} else {
					resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, 
							e.getType().toString() + ": " + e.getMessage()); 
				}
			s.invalidate();
			return;
		}

		//prepare Round2 response
		JSONObject jsonRes = new JSONObject();
		try {
			jsonRes.put("SRP_M2", srpCtx.getM2().toString());
		} catch (JSONException e) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Server: error while parsing JSON response parameters for round 2" ); 
			s.invalidate();
			return;
		}

		resp.setCharacterEncoding("utf8");
		resp.setContentType("application/json"); 
		PrintWriter out = resp.getWriter();      
		out.print(jsonRes.toString());
		//TODO: out.flush?
		out.close(); 

		//persist user id and key in the session
		s.setAttribute("SRP_CURRENT_USER_ID", srpCtx.getUserId().toString());
		s.setAttribute("SRP_CURRENT_USER_SESSKEY",srpCtx.getKey());

		//remove temporary data from the session
		s.removeAttribute("SRP_SERVER_CONTEXT");

		//TODO: remove also GAE persisted session datastore entry

		System.out.println("KEY: " + srpCtx.getKey().toString());//TODO:remove me!! **************

	}

}