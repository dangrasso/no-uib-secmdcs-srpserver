package no.uib.secmdcs.srpserver.servlet;


import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.uib.secmdcs.srpserver.crypto.SrpServerProviderFactory;
import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;
import no.uib.secmdcs.srpserver.persistence.dao.SrpUserAccountDao;
/**
 * Servlet used to register a new srp user account
 * @author daniele
 *
 */
public class RegisterUserServlet extends HttpServlet {

	private static final long serialVersionUID = -2038303224966381851L;

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		System.out.println("Creating new user ");  //TODO replace with loggers

		String userId = req.getParameter("userId");  //TODO must specify charset?
		String password = req.getParameter("password");

		System.out.println("userId: " + userId);  //TODO replace with loggers
		System.out.println("password: " + password);  //TODO replace with loggers

		ISrpServerProvider srp = null;
		//create the SRP provider
		srp = SrpServerProviderFactory.getSrpServerProvider(); 

		byte[] salt = srp.generateSalt();
		System.out.println("salt: " + Arrays.toString(salt));  //TODO replace with loggers

		BigInteger verifier = srp.generateVerifier(userId.getBytes(), password.getBytes(), salt);
		System.out.println("verifier: " + verifier.toString());  //TODO replace with loggers

		try {
			SrpUserAccountDao.INSTANCE.add(userId,verifier,salt);
		} catch (PersistenceException pe) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, pe.getMessage()); 
		}

		try {
			Thread.sleep(4000);  //wait for 4 sec for the datastore to persist
		} catch (InterruptedException e) {
		}
		resp.sendRedirect("/users.jsp");
	}
} 