package no.uib.secmdcs.srpserver.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import no.uib.secmdcs.srpserver.crypto.SrpServerContext;
import no.uib.secmdcs.srpserver.crypto.SrpServerProviderFactory;
import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;
import no.uib.secmdcs.srpserver.model.SrpUserAccount;
import no.uib.secmdcs.srpserver.persistence.dao.SrpUserAccountDao;

import org.apache.commons.codec.binary.Base64;

import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;

public class LoginRound1Servlet extends HttpServlet {

	private static final long serialVersionUID = -2296739364573682508L;

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		HttpSession s = req.getSession();
		//delete remaining auth data in the session, if any...
		s.removeAttribute("SRP_SERVER_CONTEXT");
		s.removeAttribute("SRP_CURRENT_USER_ID");
		s.removeAttribute("SRP_CURRENT_USER_SESSKEY");

		//receive userId from request JSON body
		StringBuilder sb = new StringBuilder();
	    BufferedReader reader = req.getReader();
	    try {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            sb.append(line).append('\n');
	        }
	    } finally {
	        reader.close();
	    }
	    String userId = null;
		JSONObject jsonReq;
		try {
			jsonReq = new JSONObject(sb.toString());
			//check request parameters
			if (!jsonReq.has("SRP_I")){
				resp.sendError(HttpServletResponse.SC_BAD_REQUEST,
						"Server: missing 'SRP_I' request parameters for round 1" ); 
				s.invalidate();
				return;
			}
			userId = jsonReq.getString("SRP_I");
		} catch (JSONException e) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Server: error while parsing JSON request parameters for round 1" ); 
			s.invalidate();
			return;
		} 
		
		ISrpServerProvider srp = null;
		SrpServerContext srpCtx = null;

		//create the SRP provider
		srp = SrpServerProviderFactory.getSrpServerProvider();

		//user lookup
		SrpUserAccount user = SrpUserAccountDao.INSTANCE.get(userId);
		byte[] _userId = userId.getBytes();
		if (user!=null){
			srpCtx = srp.serverRound1(_userId, user.getSalt(), user.getVerifier());
		} else{
			// to prevent an attacker to leak information about the existence of a user
			srpCtx = srp.serverMockRound1(_userId);
		}
		//save	
		s.setAttribute("SRP_SERVER_CONTEXT", srpCtx);

		//send out response data of round 1 to the client
		String _salt = Base64.encodeBase64String(srpCtx.getSalt());
		String _B = srpCtx.getBpublic().toString();
		JSONObject jsonRes = new JSONObject();
		try {
			jsonRes.put("SRP_S", _salt);
			jsonRes.put("SRP_B", _B);
		} catch (JSONException e) {
			resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Server: error while parsing JSON response parameters for round 1" ); 
			s.invalidate();
			return;
		}

		resp.setCharacterEncoding("utf8");
		resp.setContentType("application/json"); 
		PrintWriter out = resp.getWriter();      
		out.print(jsonRes.toString());
		out.flush(); //needed?
		out.close(); 
	}

}
