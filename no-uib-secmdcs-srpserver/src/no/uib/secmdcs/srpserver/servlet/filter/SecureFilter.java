package no.uib.secmdcs.srpserver.servlet.filter;

import java.io.IOException;
import java.math.BigInteger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SecureFilter implements javax.servlet.Filter {
    protected ServletContext servletContext;
 
    public void init(FilterConfig filterConfig) {
        servletContext = filterConfig.getServletContext();
    }
 
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        
    	HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        
        // read if the session contains info on the logged user
        HttpSession session = req.getSession();
        if (null != session.getAttribute("SRP_CURRENT_USER_ID")){
        	//SRP authentication already completed, we are able to decrypt the wrapped request
            
    		//Long userID = (Long) session.getAttribute("SRP_CURRENT_USER_ID");
        	String userID =  (String) session.getAttribute("SRP_CURRENT_USER_ID");
        	System.out.println("-----------*USER FOUND:     userID = " + userID ); //TODO:remove
    		
            //obtain the key used to decipher the requests
        	BigInteger sessionKey = (BigInteger) session.getAttribute("SRP_CURRENT_USER_SESSKEY");
    		System.out.println("           *            sessionKey = " + sessionKey.toString() ); //TODO:remove
    		
            //TODO:decrypt original request
            
    		//TODO:perform authorization (with role based access control) inspecting the request
            /* this can be done:
             *    -in another filter that examines the request data after it has been decrypted
             *    -before the actual decryption, to avoid it if unnecessary, using some 
             *     information in clear on the original request.
             */
    		
    		System.out.println("[securefilter: request authorized]");  //TODO:remove
    	     
            //TODO:we also need to encrypt the response
    		
            
        } else {  //the user is not already authenticated
            boolean isAuth = false;
            //TODO:perform srp mutual authentication roundtrips

            //TODO:save authenticated user id in the HTTPsession, as well as the sessionKey used for the secure channel
            
            //TODO:we can choose a sessionKey on server side and put it in the response (encrypted with the srp generated simmetric key)
         
            if (!isAuth) {
               resp.sendError(HttpServletResponse.SC_UNAUTHORIZED);
               return; //break filter chain, requested JSP/servlet will not be executed
            }
            
            /* NOTE: After successful authentication it's not clever to try to decrypt the former request: 
             * it was probably encrypted with an old session key. 
             * Or maybe is not encrypted and is just an explicit login request.
             */

            }
        
        
        //propagate to next element in the filter chain, ultimately JSP/ servlet gets executed
        chain.doFilter(request, response);        
    }
 
	@Override
	public void destroy() {
	}
 
}