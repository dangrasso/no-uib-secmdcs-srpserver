package no.uib.secmdcs.srpserver.servlet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.uib.secmdcs.srpserver.model.SrpUserAccount;
import no.uib.secmdcs.srpserver.persistence.dao.SrpUserAccountDao;

@SuppressWarnings("serial")
public class DeleteUserServlet extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
			String userId = req.getParameter("userId");  
		    System.out.println("Deleting user with id: " + userId + " ... ");  //TODO replace with loggers
		    
		    SrpUserAccount user = SrpUserAccountDao.INSTANCE.get(userId);
		    if (user!=null){
		    	SrpUserAccountDao.INSTANCE.remove(userId);
		    	System.out.println("...Deleted"); 
		    } else {
		    	System.out.println("... the user was not found!");  
		    }
		    try {
				Thread.sleep(4000);  //wait for 2 sec for the datastore to delete
			} catch (InterruptedException e) {
			}
		    resp.sendRedirect("/users.jsp");
		}
}
