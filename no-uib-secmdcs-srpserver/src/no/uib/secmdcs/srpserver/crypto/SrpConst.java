package no.uib.secmdcs.srpserver.crypto;

import java.math.BigInteger;
import java.security.SecureRandom;

import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;

public class SrpConst {
	//Some sample default values taken from the official SRP pages (bigger values can be found in RFC5054)
	public static final BigInteger DEFAULT_N_256 = new BigInteger(
			"125617018995153554710546479714086468244499594888726646874671447258204721048803");
	public static final BigInteger DEFAULT_N_512 = new BigInteger(
			"111442524391495334178357495561689917369391577789249470372002683586138633500403" +
			"39017097790259154750906072491181606044774215413467851989724116331597513345603");
	public static final BigInteger DEFAULT_N_768 = new BigInteger(
			"108717913510545785907206564905906976028054008697581762906644468236689618779357" +
			"07365745499814888682178436270948679248003428870960648442278367356671683199812" +
			"88765377499806385489913341488724152562880918438701129530606139552645689583147");
	public static final BigInteger DEFAULT_N_1024 = new BigInteger(
			"167609434410335061345139523764350090260135525329813904557420930309800865859473" +
			"55153155152380001391657389186478993474703901054632848084897951663767377660561" +
			"03746694262147761978284926913845194532182537027880222332056836358316269133571" +
			"54941914129985489522629902540768368409482248290641036967659389658897350067939");

	public static final BigInteger SRP_DEFAULT_N = DEFAULT_N_1024;
	public static final BigInteger SRP_DEFAULT_G = BigInteger.valueOf(2);
	//depending on the Hash function name H, a different digest class is used at runtime
	public static String SRP_DEFAULT_H = "SHA-512"; 

	public static final int SRP_SALT_NUMBYTES = 32; 

	//The selected provider implementation
	public static final Class<? extends ISrpServerProvider> SRP_PROVIDER_IMPL = 
			no.uib.secmdcs.srpserver.crypto.provider.bc.SrpServerProviderBCAdapter.class;

	/*
	 * This value is used to generate mock values of salt and B values when we have to 
	 * simulate the first round, after an unsuccessful userID lookup.
	 * Generating random values will give different result for the same userID, easy to spot.
	 * Generating a value directly dependent on the userID can be easily verified
	 * So we use a random seed in combination with the UserID given, that gives, at least 
	 * during the same application run, consistent values different for every username
	 */
	public static final long SRP_MOCK_VALUES_SEED = (new SecureRandom()).nextLong();
//	public static final byte[] SRP_MOCK_VALUES_SEED= new byte[SRP_SALT_NUMBYTES] ;
//	static { 
//		(new SecureRandom()).nextBytes(SRP_MOCK_VALUES_SEED);
//	}
}
