package no.uib.secmdcs.srpserver.crypto;

import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;

/**
 * A simple factory for the Srp Provider, based on a configuration property.
 * @author daniele
 *
 */
public class SrpServerProviderFactory {
	/* This factory is implemented with a static factory method rather than using a singleton, 
	 * since SrpServerProviderFactory is stateless
	 */
	public static ISrpServerProvider getSrpServerProvider() {
	try {
		return SrpConst.SRP_PROVIDER_IMPL.newInstance();
	} catch (InstantiationException e){
		throw new RuntimeException("Error while creating the SRP provider instance "
				+"from the specified class '" + SrpConst.SRP_PROVIDER_IMPL.getSimpleName() + "'");
	} catch(IllegalAccessException e) {
		throw new RuntimeException("Error while creating the SRP provider instance "
				+"from the specified class '" + SrpConst.SRP_PROVIDER_IMPL.getSimpleName() + "'");
	}
}
}
