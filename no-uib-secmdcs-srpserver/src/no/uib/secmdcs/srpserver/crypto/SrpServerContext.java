package no.uib.secmdcs.srpserver.crypto;

import java.io.Serializable;
import java.math.BigInteger;


/** This bean is used to keep track of temporary data across the 2 steps of SRP authentication 
 * 
 * @author daniele
 *
 */
public abstract class SrpServerContext implements Serializable {

	private static final long serialVersionUID = -1942435877978711865L;
	private BigInteger N;
    private BigInteger g;
    private String H;
    
    private int state; //corresponds to the number of roundtrips (0,1,2)
    
    private byte[] userId;
    private byte[] s;
    private BigInteger B;
    private boolean mockSaltAndVerifier = false;
   
    private BigInteger M2;
    private BigInteger Key;

    public SrpServerContext() {
		super();
		this.state = 0;
	}

//	public abstract BigInteger getN() ;
//	public abstract BigInteger getG(); 
//	public abstract String getH();
//	
//	public abstract int getState();
//	
//	public abstract byte[] getUserId();
//	public abstract byte[] getSalt(); 
//	public abstract BigInteger getBpublic();
//	public abstract boolean isMockSaltAndVerifier();
//	
//	public abstract BigInteger getM2(); 
//	public abstract BigInteger getKey();
//	
    
	
	public BigInteger getN() {
		return N;
	}
	public void setN(BigInteger n) {
		N = n;
	}
	public BigInteger getG() {
		return g;
	}
	public void setG(BigInteger g) {
		this.g = g;
	}
	public String getH() {
		return H;
	}
	public void setH(String h) {
		H = h;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public byte[] getUserId() {
		return userId;
	}
	public void setUserId(byte[] userId) {
		this.userId = userId;
	}
	public byte[] getSalt() {
		return s;
	}
	public void setSalt(byte[] s) {
		this.s = s;
	}
	public BigInteger getBpublic() {
		return B;
	}
	public void setBpublic(BigInteger B) {
		this.B = B;
	}
	public boolean isMockSaltAndVerifier() {
		return mockSaltAndVerifier;
	}
	public void setMockSaltAndVerifier(boolean mockSaltAndVerifier) {
		this.mockSaltAndVerifier = mockSaltAndVerifier;
	}
	public BigInteger getM2() {
		return M2;
	}
	public void setM2(BigInteger M2) {
		this.M2 = M2;
	}
	public BigInteger getKey() {
		return Key;
	}
	public void setKey(BigInteger key) {
		this.Key = key;
	}
    
    
}
