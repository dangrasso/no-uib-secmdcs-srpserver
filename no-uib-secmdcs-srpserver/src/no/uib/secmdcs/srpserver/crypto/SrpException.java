package no.uib.secmdcs.srpserver.crypto;


public class SrpException extends Exception {
	
	private static final long serialVersionUID = 8643031857817043222L;

	public SrpException(String message, Type type) {
		super(message);
		this.type=type;
	}

	public SrpException(Type type) {
		this.type=type;
	}

	private Type type;

	public static enum Type {
		INVALID_A_VALUE,
		INVALID_B_VALUE,
		INVALID_CREDENTIALS,
		INVALID_STATE
	}

	public Type getType() {
		return type;
	}
	
	
}
