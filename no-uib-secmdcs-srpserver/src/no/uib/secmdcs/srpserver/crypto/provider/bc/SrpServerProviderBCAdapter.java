package no.uib.secmdcs.srpserver.crypto.provider.bc;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import no.uib.secmdcs.srpserver.crypto.SrpConst;
import no.uib.secmdcs.srpserver.crypto.SrpException;
import no.uib.secmdcs.srpserver.crypto.SrpServerContext;
import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.agreement.srp.SRP6VerifierGenerator;

/**
 * Provider for the server-side SRP authentication, based on BouncyCastle (BC)
 * @author daniele
 */
public class SrpServerProviderBCAdapter implements ISrpServerProvider {
	private static final BigInteger N =SrpConst.SRP_DEFAULT_N;
	private static final BigInteger g =SrpConst.SRP_DEFAULT_G;
	private static final String H =SrpConst.SRP_DEFAULT_H;

	private SecureRandom random;
	private Digest digest;
	private SRP6ServerCustom serverBC;	
	private SRP6VerifierGenerator genBC;

	public SrpServerProviderBCAdapter() throws SrpException {
		random = new SecureRandom();
		digest = DigestFactoryBC.getDigest(H);
	}
	
	/* (non-Javadoc)
	 * @see no.uib.secmdcs.srpserver.crypto.providers.bc.ISrpProvider#generateSalt()
	 */
	@Override
	public byte[] generateSalt() {
		byte[] salt = new byte[SrpConst.SRP_SALT_NUMBYTES];
		random.nextBytes(salt);
		return salt;
	}

	/* (non-Javadoc)
	 * @see no.uib.secmdcs.srpserver.crypto.providers.bc.ISrpProvider#generateVerifier(byte[], byte[], java.math.BigInteger)
	 */
	@Override
	public BigInteger generateVerifier(byte[] userId, byte[] password, byte[] salt) {
		genBC = new SRP6VerifierGenerator();
		genBC.init(N,g,digest);
		BigInteger v = genBC.generateVerifier(salt, userId, password);
		return v;
	}

	/* (non-Javadoc)
	 * @see no.uib.secmdcs.srpserver.crypto.providers.bc.ISrpProvider#serverRound1(byte[], java.math.BigInteger, java.math.BigInteger)
	 */
	@Override
	public SrpServerContext serverRound1(byte[] userId, byte[] salt, BigInteger v) {
		serverBC = new SRP6ServerCustom();
		serverBC.init(N, g, v, digest, random);
		serverBC.generateServerCredentials();
		SrpServerContextBC ctx = serverBC.getSrpServerContext();
		ctx.setUserId(userId);
		ctx.setSalt(salt);
		ctx.setMockSaltAndVerifier(false);
		ctx.setState(1);
		return ctx;
	}

	/* (non-Javadoc)
	 * @see no.uib.secmdcs.srpserver.crypto.providers.bc.ISrpProvider#serverMockRound1(byte[])
	 */
	@Override
	public SrpServerContext serverMockRound1(byte[] wrongUserId) {
		serverBC = new SRP6ServerCustom();
		//generate seeded random salt (mockSalt)
		Random falseRandom = new Random(SrpConst.SRP_MOCK_VALUES_SEED);
		byte[] mockSalt = new byte[SrpConst.SRP_SALT_NUMBYTES];
		falseRandom.nextBytes(mockSalt);
	
		//generate mock verifier using the mock salt, the wrong username and a random password
		byte[] mockPassword = new byte[10]; 
		random.nextBytes(mockPassword);
		BigInteger mockVerifier = generateVerifier(wrongUserId, mockPassword, mockSalt);
		
		//perform normal round1 operations
		serverBC.init(N, g, mockVerifier, digest, random);
		serverBC.generateServerCredentials();
		SrpServerContextBC ctx = serverBC.getSrpServerContext();
		ctx.setUserId(wrongUserId);
		ctx.setSalt(mockSalt);
		ctx.setMockSaltAndVerifier(true);
		ctx.setState(1);
		return ctx;
	}

	/* (non-Javadoc)
	 * @see no.uib.secmdcs.srpserver.crypto.providers.bc.ISrpProvider#serverRound2(java.math.BigInteger, java.math.BigInteger, no.uib.secmdcs.srpserver.crypto.SrpServerContext)
	 */
	@Override
	public SrpServerContext serverRound2(BigInteger A, BigInteger M1, SrpServerContext round1Context) throws SrpException  {
		SrpServerContextBC round1ContextBC = (SrpServerContextBC) round1Context;
		
		serverBC = new SRP6ServerCustom();
		serverBC.init(round1ContextBC, digest, random);
		// Check current state valid
		if (round1ContextBC.getState() != 1)
			throw new SrpException("SrpServerContext must be in state 1 to perform round 2", SrpException.Type.INVALID_STATE);

		// Check A valid and calculate secret S
		try{
			serverBC.calculateSecret(A); // we don't need the secret's value here
		} catch (CryptoException ce1) {
			throw new SrpException(ce1.getMessage(),SrpException.Type.INVALID_A_VALUE);
		}
		
		// Stop here and report failure if the userID was wrong in the previous round
			if (round1ContextBC.isMockSaltAndVerifier())
				throw new SrpException(SrpException.Type.INVALID_CREDENTIALS);

		try{
			//Check client evidence message M1
			if (!serverBC.verifyClientEvidenceMessage(M1))
				throw new SrpException(SrpException.Type.INVALID_CREDENTIALS);
			round1ContextBC.setM2(serverBC.calculateServerEvidenceMessage());
			round1ContextBC.setKey(serverBC.calculateSessionKey());
			round1ContextBC.setState(2) ;
		} catch (CryptoException ce2) {
			throw new SrpException(ce2.getMessage(), SrpException.Type.INVALID_STATE);
		}
		
		return round1Context;
	}
}

