package no.uib.secmdcs.srpserver.crypto.provider.bc;

import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA512Digest;

/**
 * Class needed to create a Digest from the algorithm name.
 * The class also checks that the chosen algorithm is among 
 * the ones supported by this provider, and suitable for SRP.
 * @author daniele
 */
public class DigestFactoryBC {

	public static Digest getDigest(String digestName){
		Digest digest = null;
		
		switch (digestName) {
		case "SHA-1":
			digest = new SHA1Digest();
			break;
		case "SHA-224":
			digest = new SHA224Digest();
			break;
		case "SHA-256":
			digest = new SHA256Digest();
			break;
		case "SHA-384":
			digest = new SHA384Digest();
			break;
		case "SHA-512":
			digest = new SHA512Digest();
			break;
		default:
			digest = null;
			break;
		}
		if (digest==null){
			throw new RuntimeException(	"The specified Digest algorithm '" + digestName 
									+ "' is not supported.");
		}
		return digest;
		
	}
}
