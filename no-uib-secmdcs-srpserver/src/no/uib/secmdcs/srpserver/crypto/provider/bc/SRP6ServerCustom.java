package no.uib.secmdcs.srpserver.crypto.provider.bc;
import java.math.BigInteger;
import java.security.SecureRandom;
import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.agreement.srp.SRP6Server;

/**
Custom extension of the BouncyCastle's default org.bouncycastle.crypto.agreement.srp.SRP6Server 
 * to expose a context object, needed for persisting and loading temp data accross the 2 roundtrips.
 * M1, M2, Key calculation methods were also added in this extension.
 * 
 * @author Daniele Grasso
 */
public class SRP6ServerCustom extends SRP6Server {

	private BigInteger M1;
	private BigInteger M2;
	private BigInteger Key;

	public SRP6ServerCustom(){
		super();
	}
	
	/**
	 * Extension of the init method that also sets values N,g,v,b,B contained in the context 
	 * @param ctx The context that contains server side temporary values   
	 * @param digest The digest algorithm associated with the client's verifier
	 * @param random For key generation
	 */
	public void init(SrpServerContextBC ctxBC, Digest digest, SecureRandom random) {
		super.init(ctxBC.getN(), ctxBC.getG(), ctxBC.getV(), digest, random);
		if (ctxBC.getBprivate()!=null) 
			this.b = ctxBC.getBprivate();
		if (ctxBC.getBpublic()!=null) 
			this.B = ctxBC.getBpublic();		
	}

	public SrpServerContextBC getSrpServerContext() {
		SrpServerContextBC ctx = new SrpServerContextBC();
		ctx.setN(N); 
		ctx.setG(g); 
		ctx.setV(v);
		ctx.setBprivate(b);
		ctx.setBpublic(B);
		
//		ctx.setUserId(userId)
//		ctx.setA();
//		ctx.setU(u);
//		ctx.setSecret(S);
//		ctx.setM1(M1);
//		ctx.setM2(M2);
//		ctx.setKey(Key);
		
		return ctx;
	}

	/** Authenticates the received client evidence message M1 and saves it only if correct.
	 *  To be called after calculating the secret S.
	 * @param M1: the client side generated evidence message
	 * @return A boolean indicating if the client message M1 was the expected one.
	 * @throws CryptoException 
	 */
	public boolean verifyClientEvidenceMessage(BigInteger clientM1) throws CryptoException{
		//verify pre-requirements
		if ((this.A==null)||
			(this.B==null)||
			(this.S==null)){
			throw new CryptoException("Impossible to compute and verify M1: some data are missing from the previous operations (A,B,S)");
		}
		// Compute the own client evidence message 'M1'
		BigInteger computedM1 = SRP6UtilCustom.calculateM1(digest, N, A, B, S);
		if (computedM1.equals(clientM1)){
			this.M1 = clientM1;
			return true;
		}
		return false;
	}
	
	/**
	 * Computes the server evidence message M2 using the previously verified values.
	 * To be called after successfully verifying the client evidence message M1.
	 * @return M2: the server side generated evidence message
	 * @throws CryptoException
	 */
	public BigInteger calculateServerEvidenceMessage() throws CryptoException{
		//verify pre-requirements
		if ((this.A==null)||
			(this.M1==null)||
			(this.S==null)){
			throw new CryptoException("Impossible to compute M2: some data are missing from the previous operations (A,M1,S)");
		}
		// Compute the server evidence message 'M2'
		this.M2 = SRP6UtilCustom.calculateM2(digest, N, A, M1, S);  
		return M2;
	}
	
	/**
	 * Computes the final session key as a result of the SRP successful mutual authentication
	 * To be called after calculating the server evidence message M2.
	 * @return Key: the mutual authenticated symmetric session key
	 * @throws CryptoException
	 */
	public BigInteger calculateSessionKey() throws CryptoException{
		//verify pre-requirements
		if ((this.S==null)||
			(this.M1==null)||
			(this.M2==null)){
			throw new CryptoException("Impossible to compute Key: some data are missing from the previous operations (S,M1,M2)");
		}
		this.Key = SRP6UtilCustom.calculateKey(digest, N, S);
		return Key;
	}
	
	
}
