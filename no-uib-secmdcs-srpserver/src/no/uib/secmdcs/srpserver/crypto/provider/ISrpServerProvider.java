package no.uib.secmdcs.srpserver.crypto.provider;

import java.math.BigInteger;

import no.uib.secmdcs.srpserver.crypto.SrpException;
import no.uib.secmdcs.srpserver.crypto.SrpServerContext;


public interface ISrpServerProvider {

	public abstract byte[] generateSalt();

	public abstract BigInteger generateVerifier(byte[] userId, byte[] password,
			byte[] salt);

	public abstract SrpServerContext serverRound1(byte[] userId, byte[] s,
			BigInteger v);

	public abstract SrpServerContext serverMockRound1(byte[] wrongUserId);

	public abstract SrpServerContext serverRound2(BigInteger A, BigInteger M1,
			SrpServerContext round1Context) throws SrpException;

}