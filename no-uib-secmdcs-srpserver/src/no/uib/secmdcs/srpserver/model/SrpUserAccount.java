package no.uib.secmdcs.srpserver.model;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Model class representing a user in a Secure Remote Password authenticated system.
 * @author daniele
 *
 */
@Entity
public class SrpUserAccount {
@Id
private String userId;
private BigInteger verifier;
private byte[] salt;

public SrpUserAccount(String userId, BigInteger verifier, byte[] salt){
	this.userId = userId;
	this.verifier = verifier;
	this.salt = salt;		
}

public  String getUserId() {
	return userId;
}

public void setUserId( String userId) {
	this.userId = userId;
}

public  BigInteger getVerifier() {
	return verifier;
}

public void setVerifier( BigInteger verifier) {
	this.verifier = verifier;
}

public  byte[] getSalt() {
	return salt;
}

public void setSalt( byte[] salt) {
	this.salt = salt;
}


}
