<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ page import="java.util.List"%>
<%@ page import="no.uib.secmdcs.srpserver.model.SrpUserAccount"%>
<%@ page import="no.uib.secmdcs.srpserver.persistence.dao.SrpUserAccountDao"%>

<!DOCTYPE html>


<%@page import="java.util.ArrayList"%>

<html>
<head>
<title>Users</title>
<link rel="stylesheet" type="text/css" href="css/main.css" />
<meta charset="utf-8">
</head>
<body>
	<%
		SrpUserAccountDao dao = SrpUserAccountDao.INSTANCE;
		List<SrpUserAccount> users = new ArrayList<SrpUserAccount>();
		users = dao.listUserAccounts();
	%>
	<div style="width: 100%;">
		<div class="line"></div>
		<div class="topLine">
			<div style="float: left;" class="headline">SRP Users</div>

		</div>
	</div>

	<div style="clear: both;" />
	You have a total number of
	<%=users.size()%>
	Users.

	<table class="fullpaged">
		<tr>
			<th>userId</th>
			<th style="width:50%;">verifier</th>
			<th style="width:25%;">salt</th>
			<th style="width:60px;"></th>
			
		</tr>

		<%
			for (SrpUserAccount user : users) {
		%>
		<tr>
			<td>
			<%=user.getUserId()%>
			</td>
			<td>
			<%=user.getVerifier()%></td>
			<td>
			<%=new String(user.getSalt())%>
			</td>
			<td >
				<form action="/deleteuser" method="post">
					<input type="hidden" name="userId" id="userId" value="<%=user.getUserId()%>" /> <input
						type="submit" value="Delete" />
				</form>
			</td>
		</tr>
		<%
			}
		%>
	</table>


	<hr />

	<div class="main">

		<div class="headline">Register New User</div>

		<form action="/registeruser" method="post" accept-charset="utf-8">
			<table>
				<tr>
					<td><label for="userId">userId</label></td>
					<td><input type="text" name="userId" id="userId" size="70" /></td>
				</tr>
				<tr>
					<td valign="top"><label for="password">password</label></td>
					<td><input type="text" name="password" id="password" size="70" /></td>
				</tr>
				<tr>
					<td colspan="3" align="left"><input type="submit"
						value="Register" /></td>
				</tr>
			</table>
		</form>

	</div>
</body>
</html>
