package no.uib.secmdcs.srpserver.crypto.provider.bc;

import static org.junit.Assert.*;

import java.math.BigInteger;

import no.uib.secmdcs.srpserver.crypto.SrpException;
import no.uib.secmdcs.srpserver.crypto.SrpServerContext;
import no.uib.secmdcs.srpserver.crypto.provider.ISrpServerProvider;
import no.uib.secmdcs.srpserver.crypto.provider.bc.SRP6UtilCustom;
import no.uib.secmdcs.srpserver.crypto.provider.bc.SrpServerContextBC;
import no.uib.secmdcs.srpserver.crypto.provider.bc.SrpServerProviderBCAdapter;
import no.uib.secmdcs.srpserver.util.Hex;

import org.bouncycastle.crypto.digests.SHA1Digest;
import org.junit.Assert;
import org.junit.Test;


public class SrpServerProviderBCAdapterTest {
	//Test vectors as in RFC5054, Appendix B
	byte[] userId = "alice".getBytes();
	byte[] password = "password123".getBytes();
	byte[] salt = Hex.toByteArray("BEB25379D1A8581EB5A727673A2441EE");
	BigInteger  N =  Hex.toBigInteger(
			"EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C9C256576" +
					"D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE48E495C1D6089DAD1" +
					"5DC7D7B46154D6B6CE8EF4AD69B15D4982559B297BCF1885C529F566660E57EC" +
			"68EDBC3C05726CC02FD4CBF4976EAA9AFD5138FE8376435B9FC61D2FC0EB06E3");
	BigInteger  g = new BigInteger("2");
	BigInteger  expectedK =  Hex.toBigInteger("7556AA045AEF2CDD07ABAF0F665C3E818913186F");
	BigInteger  expectedX =  Hex.toBigInteger("94B7555AABE9127CC58CCF4993DB6CF84D16C124");
	BigInteger  expectedV =  Hex.toBigInteger(
			"7E273DE8696FFC4F4E337D05B4B375BEB0DDE1569E8FA00A9886D8129BADA1F1" +
					"822223CA1A605B530E379BA4729FDC59F105B4787E5186F5C671085A1447B52A" +
					"48CF1970B4FB6F8400BBF4CEBFBB168152E08AB5EA53D15C1AFF87B2B9DA6E04" +
			"E058AD51CC72BFC9033B564E26480D78E955A5E29E7AB245DB2BE315E2099AFB");
	BigInteger a = Hex.toBigInteger(
			"60975527035CF2AD1989806F0407210BC81EDC04E2762A56AFD529DDDA2D4393");
	BigInteger b = Hex.toBigInteger(
			"E487CB59D31AC550471E81F00F6928E01DDA08E974A004F49E61F5D105284D20");
	BigInteger expectedA =  Hex.toBigInteger(
			"61D5E490F6F1B79547B0704C436F523DD0E560F0C64115BB72557EC44352E890" +
					"3211C04692272D8B2D1A5358A2CF1B6E0BFCF99F921530EC8E39356179EAE45E" +
					"42BA92AEACED825171E1E8B9AF6D9C03E1327F44BE087EF06530E69F66615261" +
			"EEF54073CA11CF5858F0EDFDFE15EFEAB349EF5D76988A3672FAC47B0769447B");
	BigInteger expectedB = Hex.toBigInteger(
			"BD0C61512C692C0CB6D041FA01BB152D4916A1E77AF46AE105393011BAF38964" +
					"DC46A0670DD125B95A981652236F99D9B681CBF87837EC996C6DA04453728610" +
					"D0C6DDB58B318885D7D82C7F8DEB75CE7BD4FBAA37089E6F9C6059F388838E7A" +
			"00030B331EB76840910440B1B27AAEAEEB4012B7D7665238A8E3FB004B117B58");
	BigInteger expectedU = Hex.toBigInteger( 
			"CE38B9593487DA98554ED47D70A7AE5F462EF019");
	BigInteger expectedSecret = Hex.toBigInteger(
			"B0DC82BABCF30674AE450C0287745E7990A3381F63B387AAF271A10D233861E3" +
					"59B48220F7C4693C9AE12B0A6F67809F0876E2D013800D6C41BB59B6D5979B5C" +
					"00A172B4A2A5903A0BDCAF8A709585EB2AFAFA8F3499B200210DCC1F10EB3394" +
			"3CD67FC88A2F39A4BE5BEC4EC0A3212DC346D7E474B29EDE8A469FFECA686E5A");


	@Test
	public void test() {
		ISrpServerProvider srp = null;
		try {
			srp = new SrpServerProviderBCAdapter();
		} catch (SrpException se) {
			fail(se.getType().toString() + ": " + se.getMessage());
		}
		
		//test provider construction
		Assert.assertNotNull("Provider not null",srp);

		//test verifier generation
		BigInteger actualV = srp.generateVerifier(userId, password, salt);
		Assert.assertEquals("Verifier correct",expectedV, actualV);

		//test round1
		SrpServerContextBC ctxRound1 = (SrpServerContextBC) srp.serverRound1(userId, salt, actualV);
		Assert.assertEquals("State updated after Round1",ctxRound1.getState(),1);
		Assert.assertNotNull("b not null after Round1",ctxRound1.getBprivate());
		Assert.assertNotNull("B not null after Round1",ctxRound1.getBpublic());
		//test the byte size of the generated b and B values, against the test ones
	//	Assert.assertTrue("b matching the expected size after Round1",ctxRound1.getBprivate().toByteArray().length==b.toByteArray().length);
	//	Assert.assertTrue("B matching the expected size after Round1",ctxRound1.getBpublic().toByteArray().length==expectedB.toByteArray().length);

		//test round 1 (mock version)
		SrpServerContextBC ctxRound1Mock = (SrpServerContextBC) srp.serverMockRound1("ThisIsAwrongUserName".getBytes());
		Assert.assertEquals("State updated after (mock)Round1", ctxRound1Mock.getState(),1);
		Assert.assertNotNull("b not null after (mock)Round1",ctxRound1Mock.getBprivate());
		Assert.assertNotNull("B not null after (mock)Round1",ctxRound1Mock.getBpublic());
		//test the byte size of the generated b and B values, against the test ones
	//	Assert.assertTrue("b matching the expected size after (mock)Round1",ctxRound1Mock.getBprivate().toByteArray().length==b.toByteArray().length);
	//	Assert.assertTrue("B matching the expected size after (mock)Round1",ctxRound1Mock.getBpublic().toByteArray().length==expectedB.toByteArray().length);


		//test round 2
		/*NOTE: 
		 * We want to use predefined values for b and B, so we overwrite 
		 * them in the context, to replace the new randomly generated ones.
		 */
		ctxRound1.setBprivate(b);
		ctxRound1.setBpublic(expectedB);
		SrpServerContext ctxRound2 = null;

		BigInteger expectedM1 = SRP6UtilCustom.calculateM1(new SHA1Digest(), N, expectedA, expectedB, expectedSecret);
		System.out.println("---> expectedM1 (BigInteger): " + expectedM1);

		try {	
			ctxRound2 = srp.serverRound2(expectedA, expectedM1, ctxRound1);
		} catch (SrpException e) {
			fail(e.getType().toString() + ": " + e.getMessage());
		}

		Assert.assertEquals("State updated after Round2", ctxRound2.getState(),2);

		BigInteger expectedM2 = SRP6UtilCustom.calculateM2(new SHA1Digest(), N, expectedA, expectedM1, expectedSecret);
		System.out.println("---> expectedM2 (BigInteger): " + expectedM2);

		Assert.assertNotNull("M2 not null", ctxRound2.getM2());
		Assert.assertEquals("M2 correct", ctxRound2.getM2(),expectedM2);

		BigInteger expectedKey = SRP6UtilCustom.calculateKey(new SHA1Digest(), N, expectedSecret);
		System.out.println("--> expectedKey (BigInteger): " + expectedKey);

		Assert.assertNotNull("Session Key not null", ctxRound2.getKey());
		Assert.assertEquals("Key correct", ctxRound2.getKey(), expectedKey);

	}

}
