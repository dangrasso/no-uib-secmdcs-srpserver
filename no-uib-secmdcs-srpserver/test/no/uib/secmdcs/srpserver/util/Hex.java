package no.uib.secmdcs.srpserver.util;

import java.math.BigInteger;

import javax.xml.bind.DatatypeConverter;

public class Hex {
	
	public static byte[] toByteArray(String hex){
		return	DatatypeConverter.parseHexBinary(hex);
	}
	
	public static BigInteger toBigInteger(String hex){
		return new BigInteger(1,toByteArray(hex));
	}
	
}
