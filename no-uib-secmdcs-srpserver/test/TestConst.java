import java.math.BigInteger;

import org.bouncycastle.crypto.agreement.srp.SRP6Util;
import org.bouncycastle.crypto.agreement.srp.SRP6VerifierGenerator;

import org.bouncycastle.crypto.digests.SHA1Digest;

import no.uib.secmdcs.srpserver.crypto.SrpConst;
import no.uib.secmdcs.srpserver.util.Hex;


public class TestConst {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Test vectors as in RFC5054, Appendix B
		byte[] userId = "alice".getBytes();
		byte[] password = "password123".getBytes();
		byte[] salt = Hex.toByteArray("BEB25379D1A8581EB5A727673A2441EE");
		BigInteger  N =  Hex.toBigInteger(
				"EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C9C256576" +
						"D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE48E495C1D6089DAD1" +
						"5DC7D7B46154D6B6CE8EF4AD69B15D4982559B297BCF1885C529F566660E57EC" +
				"68EDBC3C05726CC02FD4CBF4976EAA9AFD5138FE8376435B9FC61D2FC0EB06E3");
		BigInteger  g = new BigInteger("2");
		BigInteger  expectedK =  Hex.toBigInteger("7556AA045AEF2CDD07ABAF0F665C3E818913186F");
		BigInteger  expectedX =  Hex.toBigInteger("94B7555AABE9127CC58CCF4993DB6CF84D16C124");
		BigInteger  expectedV =  Hex.toBigInteger(
				"7E273DE8696FFC4F4E337D05B4B375BEB0DDE1569E8FA00A9886D8129BADA1F1" +
						"822223CA1A605B530E379BA4729FDC59F105B4787E5186F5C671085A1447B52A" +
						"48CF1970B4FB6F8400BBF4CEBFBB168152E08AB5EA53D15C1AFF87B2B9DA6E04" +
				"E058AD51CC72BFC9033B564E26480D78E955A5E29E7AB245DB2BE315E2099AFB");
		BigInteger a = Hex.toBigInteger(
				"60975527035CF2AD1989806F0407210BC81EDC04E2762A56AFD529DDDA2D4393");
		BigInteger b = Hex.toBigInteger(
				"E487CB59D31AC550471E81F00F6928E01DDA08E974A004F49E61F5D105284D20");
		BigInteger expectedA =  Hex.toBigInteger(
				"61D5E490F6F1B79547B0704C436F523DD0E560F0C64115BB72557EC44352E890" +
						"3211C04692272D8B2D1A5358A2CF1B6E0BFCF99F921530EC8E39356179EAE45E" +
						"42BA92AEACED825171E1E8B9AF6D9C03E1327F44BE087EF06530E69F66615261" +
				"EEF54073CA11CF5858F0EDFDFE15EFEAB349EF5D76988A3672FAC47B0769447B");
		BigInteger expectedB = Hex.toBigInteger(
				"BD0C61512C692C0CB6D041FA01BB152D4916A1E77AF46AE105393011BAF38964" +
						"DC46A0670DD125B95A981652236F99D9B681CBF87837EC996C6DA04453728610" +
						"D0C6DDB58B318885D7D82C7F8DEB75CE7BD4FBAA37089E6F9C6059F388838E7A" +
				"00030B331EB76840910440B1B27AAEAEEB4012B7D7665238A8E3FB004B117B58");
		BigInteger expectedU = Hex.toBigInteger( 
				"CE38B9593487DA98554ED47D70A7AE5F462EF019");
		

		System.out.println("------------PARAMETER NAME---------------");
		System.out.println("-(Expected value)");
		System.out.println("-(Actual value)");
		System.out.println("-----------------------------------------");
		System.out.println("---Test parameter N (1024):  ");
		System.out.println(N);
		System.out.println(SrpConst.DEFAULT_N_1024);
		
		System.out.println("---Test parameter g       :  " );
		System.out.println(g);
		System.out.println(SrpConst.SRP_DEFAULT_G);
		
		System.out.println("---Test parameter salt    :  ");
		System.out.println(salt);
		
		System.out.println("---Test parameter x       :  " );
		System.out.println(expectedX);
		System.out.println(SRP6Util.calculateX(new SHA1Digest(), N, salt, userId, password));
		
		
		SRP6VerifierGenerator gen = new SRP6VerifierGenerator();
		gen.init(N, g, new SHA1Digest());
		System.out.println("---Test parameter v       :  ");
		System.out.println(expectedV);
		System.out.println(gen.generateVerifier( salt, userId, password));
		
		System.out.println("---Test parameter k       :  " + expectedK);
		System.out.println("---Test parameter a       :  " + a);
		System.out.println("---Test parameter b       :  " + b);
		System.out.println("---Test parameter A       :  " + expectedA);
		System.out.println("---Test parameter B       :  " + expectedB);
		System.out.println("---Test parameter u       :  " + expectedU);
		//System.out.println("---Test parameter A       :  " + A);

	}

}
